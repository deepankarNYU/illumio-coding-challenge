Illumio Coding Assignment

Installation and Running

Install the requirements by running:

$ pip install -r requirements.txt
To run the program (provided csv file), please execute (make sure you are using Python 3 as the default Python):

$ python firewall.py f_data.csv
To run the program with your csv file, please execute:

$ python firewall.py [filename]
To run the tests on python (this will automatically use the test csv file) , please execute,

$ pytest -q firewall_test.py 
Insights

I chose to use data frames and pandas since I have just started with my machine learning course at NYU.I feel data frames make this assignment concise and quick. I am using the method of elimination to filter out data which do not match the data within the data frame.

Team

I am really interested in working as an intern with the Data team, more specifically Data processing and analysis. My interest lies in data discovery. I believe I can apply and hone my existing skills in data analytics further.